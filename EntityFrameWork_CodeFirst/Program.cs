﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameWork_CodeFirst
{
    //before creating the classe don't forget to install entity framework "install-package EntityFramwork" in the NugetPackage Manager
    public class Post
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
    }
    //enable code first migration in the NugetPackage manager "enable-migrations"
    //don't forget to a connection string to the database

    public class BlogDbContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
    }


    //in the NugetPackage manager we hav to also type "add-migration CreatePost"
    // Your migration files are created in the Migrations, with a date and 2 functions to create and drop database
    // then run again in the NugetPackage manager and run "update-Database" to create the table
    class Program
    {
        static void Main(string[] args)
        {

        }
    }
}
